/*
 * lib.rs
 *
 * ais-discovery - Implementation of the AIS Discovery Protocol
 * Copyright (C) 2018 Standard Mining
 *
 * ais-discovery is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * All rights reserved.
 */

extern crate bincode;
extern crate hostname;

#[macro_use]
extern crate lazy_static;

#[macro_use]
extern crate log;
extern crate parking_lot;
extern crate regex;

#[macro_use]
extern crate serde;
extern crate void;

mod error;
mod models;
mod response;
mod solicit;
mod util;
mod wildcard;

pub use self::error::Error;
pub use self::models::*;
pub use self::response::Responder;
pub use self::solicit::Solicitor;

use std::net::Ipv6Addr;

lazy_static! {
    pub static ref MULTICAST_ADDRESS: Ipv6Addr = {
        "ff02:6d61::1".parse().unwrap()
    };
}

pub const MULTICAST_SOLICIT_PORT: u16 = 9701;
pub const MULTICAST_RECEIVE_PORT: u16 = 9702;

pub const MAX_SERVICE_NAME: usize = 256;

pub type StdResult<T, E> = std::result::Result<T, E>;
pub type Result<T> = StdResult<T, Error>;
