/*
 * util.rs
 *
 * ais-discovery - Implementation of the AIS Discovery Protocol
 * Copyright (C) 2018 Standard Mining
 *
 * ais-discovery is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * All rights reserved.
 */

pub mod thread {
    pub use std::thread::{current, park, sleep, yield_now, Builder, JoinHandle, LocalKey, Thread,
                          ThreadId};

    #[inline]
    pub fn spawn<S, F, T>(name: S, f: F) -> JoinHandle<T>
    where
        S: Into<String>,
        F: FnOnce() -> T,
        F: Send + 'static,
        T: Send + 'static,
    {
        Builder::new().name(name.into()).spawn(f).unwrap()
    }
}
