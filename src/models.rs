/*
 * models.rs
 *
 * ais-discovery - Implementation of the AIS Discovery Protocol
 * Copyright (C) 2018 Standard Mining
 *
 * ais-discovery is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * All rights reserved.
 */

use {bincode, Error, Result};
use hostname::get_hostname;
use std::net::SocketAddrV6;

lazy_static! {
    pub static ref HOSTNAME: String = {
        // There don't seem to be any errors possible
        // based on the gethostname(3p) man page.
        get_hostname().unwrap()
    };
}

pub const MAGIC_BYTES: u64 = 0x02a1_5d50_6d61_0008;
pub const API_VERSION: u8 = 0;

#[derive(Serialize, Deserialize, Debug, Clone, PartialEq)]
pub struct Request {
    pub magic: u64,
    pub api_version: u8,
    pub services: Vec<String>,
    pub application: String,
    pub version: (u16, u16, u16),
    pub address: SocketAddrV6,
}

impl Request {
    pub fn new<S: Into<String>>(
        application: S,
        version: (u16, u16, u16),
        address: SocketAddrV6,
    ) -> Self {
        Request {
            magic: MAGIC_BYTES,
            api_version: API_VERSION,
            services: Vec::new(),
            application: application.into(),
            version,
            address,
        }
    }

    pub fn validate(&self) -> Result<()> {
        if self.magic != MAGIC_BYTES {
            return Err(Error::StaticMsg("Magic bytes do not match"));
        }

        if self.api_version != API_VERSION {
            return Err(Error::Msg(format!(
                "API version doesn't match: expected {}, found {}",
                API_VERSION, self.api_version
            )));
        }

        Ok(())
    }

    #[inline]
    pub fn from_bytes(bytes: &[u8]) -> Result<Self> {
        bincode::deserialize(bytes).map_err(Error::from)
    }

    #[inline]
    pub fn to_bytes(&self) -> Result<Vec<u8>> {
        bincode::serialize(self).map_err(Error::from)
    }

    #[inline]
    pub fn to_bytes_vec(&self, buf: &mut Vec<u8>) -> Result<()> {
        bincode::serialize_into(buf, self).map_err(Error::from)
    }
}

#[derive(Serialize, Deserialize, Debug, Clone, PartialEq)]
pub struct Response {
    pub magic: u64,
    pub api_version: u8,
    pub service: String,
    pub port: u16,
    pub application: String,
    pub version: (u16, u16, u16),
    pub hostname: String,
}

impl Response {
    pub fn new(application: &str, version: (u16, u16, u16)) -> Self {
        Response {
            magic: MAGIC_BYTES,
            api_version: API_VERSION,
            service: String::new(),
            port: 0,
            application: application.to_string(),
            version,
            hostname: HOSTNAME.clone(),
        }
    }

    pub fn validate(&self) -> Result<()> {
        if self.magic != MAGIC_BYTES {
            return Err(Error::StaticMsg("Magic bytes do not match"));
        }

        if self.api_version != API_VERSION {
            return Err(Error::Msg(format!(
                "API version doesn't match: expected {}, found {}",
                API_VERSION, self.api_version
            )));
        }

        Ok(())
    }

    #[inline]
    pub fn from_bytes(bytes: &[u8]) -> Result<Self> {
        bincode::deserialize(bytes).map_err(Error::from)
    }

    #[inline]
    pub fn to_bytes(&self) -> Result<Vec<u8>> {
        bincode::serialize(self).map_err(Error::from)
    }

    #[inline]
    pub fn to_bytes_vec(&self, buf: &mut Vec<u8>) -> Result<()> {
        bincode::serialize_into(buf, self).map_err(Error::from)
    }
}
