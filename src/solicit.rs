/*
 * solicit.rs
 *
 * ais-discovery - Implementation of the AIS Discovery Protocol
 * Copyright (C) 2018 Standard Mining
 *
 * ais-discovery is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * All rights reserved.
 */

use {Error, Result};
use models::{Request, Response, API_VERSION, MAGIC_BYTES};
use parking_lot::RwLock;
use std::net::{SocketAddr, SocketAddrV6, UdpSocket};
use std::sync::Arc;
use util::thread::{self, JoinHandle};
use void::Void;

#[derive(Debug)]
pub struct Solicitor {
    socket: UdpSocket,
    multicast_addr: SocketAddrV6,
    address: SocketAddrV6,
    responses: RwLock<Vec<(Response, SocketAddr)>>,
    application: String,
    version: (u16, u16, u16),
}

impl Solicitor {
    pub fn bind<S: Into<String>>(
        multicast_addr: SocketAddrV6,
        recv_addr: SocketAddrV6,
        application: S,
        version: (u16, u16, u16),
        interface: u32,
        time_to_live: u32,
    ) -> Result<Self> {
        if !multicast_addr.ip().is_multicast() {
            return Err(Error::StaticMsg(
                "Passed socket address is not a multicast address",
            ));
        }

        let socket = UdpSocket::bind(("::", recv_addr.port()))?;
        socket.join_multicast_v6(multicast_addr.ip(), interface)?;
        socket.set_ttl(time_to_live)?;

        Ok(Solicitor {
            socket,
            multicast_addr,
            address: recv_addr,
            responses: RwLock::new(Vec::new()),
            application: application.into(),
            version,
        })
    }

    pub fn solicit<I, S>(&self, services: I) -> Result<()>
    where
        I: IntoIterator<Item = S>,
        S: Into<String>,
    {
        let request = Request {
            magic: MAGIC_BYTES,
            api_version: API_VERSION,
            services: services.into_iter().map(|s| s.into()).collect(),
            application: self.application.clone(),
            version: self.version,
            address: self.address,
        };

        let bytes = request.to_bytes()?;
        self.socket.send_to(&bytes, &self.multicast_addr)?;
        Ok(())
    }

    pub fn receive_response(&self) -> Result<SocketAddr> {
        let mut buf = [0; 576];
        let (len, addr) = self.socket.recv_from(&mut buf[..])?;
        let slice = &buf[..len];
        let response = Response::from_bytes(slice)?;

        info!("Received response from {}: {:#?}", &addr, &response);
        response.validate()?;
        self.responses.write().push((response, addr));
        Ok(addr)
    }

    #[inline]
    pub fn multicast_addr(&self) -> &SocketAddrV6 {
        &self.multicast_addr
    }

    #[inline]
    pub fn socket(&self) -> &UdpSocket {
        &self.socket
    }

    #[inline]
    pub fn responses(&self) -> &RwLock<Vec<(Response, SocketAddr)>> {
        &self.responses
    }

    pub fn listen_loop(&self) -> ! {
        loop {
            match self.receive_response() {
                Ok(_) => (),
                Err(e) => error!("Unable to wait for response: {}", e),
            }
        }
    }

    pub fn spawn_listener(this: Arc<Self>) -> JoinHandle<Void> {
        thread::spawn("aisdp solicitor", move || this.listen_loop())
    }
}
