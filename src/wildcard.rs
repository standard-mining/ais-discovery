/*
 * wildcard.rs
 *
 * ais-discovery - Implementation of the AIS Discovery Protocol
 * Copyright (C) 2018 Standard Mining
 *
 * ais-discovery is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * All rights reserved.
 */

use {MAX_SERVICE_NAME, Error, Result};
use regex::Regex;

pub fn compile(pattern: &str, buffer: &mut String) -> Regex {
    debug!("Compiling glob pattern \"{}\"...", pattern);

    buffer.clear();
    buffer.push('^');

    for ch in pattern.chars() {
        match ch {
            '*' => buffer.push_str(".*"),
            '?' => buffer.push_str(".?"),
            'A'..='Z' | 'a'..='z' | '0'..='9' | '-' | '_' => buffer.push(ch),
            _ => debug!("Invalid character in pattern: '{}'", ch),
        }
    }

    buffer.push('$');
    Regex::new(&buffer).unwrap()
}

pub fn validate(pattern: &str) -> Result<()> {
    if pattern.len() > MAX_SERVICE_NAME {
        return Err(Error::StaticMsg("Pattern length too long"));
    }

    if pattern.chars().filter(|&c| c == '*' || c == '?').count() > 2 {
        return Err(Error::StaticMsg("Too many wildcard characters"));
    }

    Ok(())
}
