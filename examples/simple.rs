/*
 * examples/simple.rs
 *
 * ais-discovery - Implementation of the AIS Discovery Protocol
 * Copyright (C) 2018 Standard Mining
 *
 * ais-discovery is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * All rights reserved.
 */

extern crate ais_discovery;

use ais_discovery::{Responder, Solicitor};
use std::net::SocketAddrV6;
use std::sync::Arc;
use std::thread;
use std::time::Duration;

const APPLICATION: &str = "Simple Example";
const VERSION: (u16, u16, u16) = (1, 0, 0);

fn solicit(sltr: &Solicitor, services: &[&str]) {
    println!("-------------------------");
    println!("Soliciting services: {:?}", services);
    sltr.solicit(services.iter().cloned())
        .expect("Solicitation failed");

    thread::sleep(Duration::from_millis(250));
    let mut responses = sltr.responses().write();
    println!("{:#?}", &*responses);
    responses.clear();
}

fn main() {
    let mc_addr = "[ff02:6d61::1]:9701".parse::<SocketAddrV6>().unwrap();
    let lc_addr = "[::1]:9702".parse::<SocketAddrV6>().unwrap();

    println!("Creating Responder...");
    let resp = Responder::bind(
        mc_addr.clone(),
        APPLICATION,
        VERSION,
        vec![
            ("example_daemon", 1000),
            ("example_service", 2000),
            ("example_xyz", 123),
        ],
        0,
    ).expect("Unable to create Responder");

    println!("Creating Solicitor...");
    let sltr = Arc::new(
        Solicitor::bind(mc_addr, lc_addr, APPLICATION, VERSION, 0, 5)
            .expect("Unable to create Solicitor"),
    );

    println!("Spawning responder and solicitor threads...");
    resp.spawn();
    Solicitor::spawn_listener(Arc::clone(&sltr));

    solicit(&sltr, &["example_service"]);
    solicit(&sltr, &["example_daemon", "no_such_service"]);
    solicit(&sltr, &["example_xyz", "example_service"]);
}
