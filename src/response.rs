/*
 * response.rs
 *
 * ais-discovery - Implementation of the AIS Discovery Protocol
 * Copyright (C) 2018 Standard Mining
 *
 * ais-discovery is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * All rights reserved.
 */

use {wildcard, Error, Result};
use models::{Request, Response};
use std::net::{SocketAddrV6, UdpSocket};
use util::thread::{self, JoinHandle};
use void::Void;

#[derive(Debug)]
pub struct Responder {
    socket: UdpSocket,
    application: String,
    version: (u16, u16, u16),
    services: Vec<(String, u16)>,
}

impl Responder {
    pub fn bind<I, S>(
        addr: SocketAddrV6,
        application: S,
        version: (u16, u16, u16),
        services: I,
        interface: u32,
    ) -> Result<Self>
    where
        I: IntoIterator<Item = (S, u16)>,
        S: Into<String>,
    {
        if !addr.ip().is_multicast() {
            return Err(Error::StaticMsg(
                "Passed socket address is not a multicast address",
            ));
        }

        info!("Binding responder socket to [::]:{}...", addr.port());
        let socket = UdpSocket::bind(("::", addr.port()))?;
        debug!(
            "Joining IPv6 multicast {} (interface {})...",
            addr.ip(),
            interface
        );
        socket.join_multicast_v6(addr.ip(), interface)?;
        debug!("Enabling multicast loopback...");
        socket.set_multicast_loop_v6(true)?;

        Ok(Responder {
            socket,
            application: application.into(),
            version,
            services: services.into_iter().map(|(s, p)| (s.into(), p)).collect(),
        })
    }

    pub fn read(&self) -> Result<Request> {
        let mut buf = [0; 576];
        debug!("Blocking on receiver socket...");
        let (len, addr) = self.socket.recv_from(&mut buf[..])?;
        let slice = &buf[..len];
        let request = Request::from_bytes(slice)?;

        info!("Received request from {}: {:#?}", &addr, &request);
        request.validate()?;
        Ok(request)
    }

    pub fn respond(&self, request: &Request) -> Result<()> {
        let mut response = Response::new(&self.application, self.version);
        let mut string = String::new();
        let mut bytes = Vec::new();

        for pattern in &request.services {
            wildcard::validate(pattern)?;
        }

        for pattern in &request.services {
            let regex = wildcard::compile(pattern, &mut string);
            for &(ref service, port) in &self.services {
                if regex.is_match(service) {
                    response.service.clone_from(service);
                    response.port = port;
                    trace!("Modified response: {:#?}", &response);

                    debug!("Serializing service to bytes...");
                    bytes.clear();
                    response.to_bytes_vec(&mut bytes)?;

                    debug!("Writing response to {}...", &request.address);
                    self.socket.send_to(&bytes, &request.address)?;
                }
            }
        }

        Ok(())
    }

    #[inline]
    pub fn socket(&self) -> &UdpSocket {
        &self.socket
    }

    #[inline]
    pub fn application(&self) -> &str {
        &self.application
    }

    #[inline]
    pub fn version(&self) -> (u16, u16, u16) {
        self.version
    }

    #[inline]
    pub fn services(&self) -> &[(String, u16)] {
        &self.services
    }

    pub fn main_loop(&self) -> ! {
        loop {
            let request = match self.read() {
                Ok(req) => req,
                Err(e) => {
                    error!("Unable to wait for request: {}", e);
                    continue;
                }
            };

            if let Err(e) = self.respond(&request) {
                error!("Error sending response to request: {}", e);
            }
        }
    }

    pub fn spawn(self) -> JoinHandle<Void> {
        thread::spawn("aisdp responder", move || self.main_loop())
    }
}
