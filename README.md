## AIS' Internal Service Discovery Protocol
AISDP is a protocol to discover machines on a local network. Machines that wish
to have discoverable will run the **receiver** end of this protocol. They will
respond to queries on a particular IPv6 multicast network, and will send which
services match those that are requested to the **solicitor**.

For instance, John wants to play a game, and so wishes to see who on the network
is running `tictactoe`. He then solicits `["tictactoe", "chess"]`, which is broadcast on
the multicast network. All receivers, on reading his request, will send details
of the matching service(s) to the address John sent in his request.

To use, add the following to your `Cargo.toml`:
```toml
[dependencies]
ais-discovery = { git = "https://gitlab.com/standard-mining/ais-discovery" }
```

### Examples
See the source code in `examples/`. To run them:

```
cargo run --example simple
cargo run --example wildcards
```

### Network Addresses
The solicitor can seek available services on the local network. It should
listen on the address `ff02:6d61::1`. However, if the organization decides,
it may instead use `ff0x:6d61::1`, where `x` is the network scope they wish
to use. See [multicast addresses](https://en.wikipedia.org/wiki/Multicast_address#IPv6).
The solicitor should aim his multicast request to port `9701`. Part of the request
includes the address and port the solicitor is able to receive a response at.
At this location, the solicitor must set up a UDP6 socket. The recommended
port to use is `9702`. The solicitor should also set a time to live (TTL) on their
multicast to prevent requests from infinitely hogging a network.

### Request and Responses
Any serialized data is sent in the [bincode](https://github.com/TyOverby/bincode) format,
which is essentially just the information packed bytewise.

The solicitor will send a request for services, which will take the following form:
```rs
struct Request {
    magic: u64,
    api_version: u8,
    services: Vec<String>,
    application: String,
    version: (u16, u16, u16),
    address: SocketAddrV6,
}
```

Each receiving server will send a response for each service it supports.
```rs
struct Response {
    magic: u64,
    api_version: u8,
    service: String,
    port: u16,
    application: String,
    version: (u16, u16, u16),
    hostname: String,
}
```

Each UDP request or response may only be 576 bytes total.

The magic bytes must be `02 a1 5d 50 6d 61 00 08`.
The API version is `0`.

### Wildcard requests
When requesting a list of services, asterisks (`*`) and question marks (`?`)
can be added as wildcards. `*` will represent the "zero or more characters",
while `?` will represent an "optional character". Responders will match their
hosted services against the ones requested.

To limit the computational time for responders, services with more than two
wildcards will not be processed.

### Service Names
Services shall only have names that correspond to the regular expression
`[A-Za-z0-9][A-Za-z0-9_-]*`. Any service names that do not correspond can
be discarded.

They shall not be any longer than 256 bytes.

## Appendix
The version field is a non-string semantic version of the application. It is
meant to be approximate, so that clients can determine whether they think
that services is too old or too new for them.

A response returns a few categories of data. It has the header to validate the
response, the machine information (hostname, application, version), but also
a service that matched one that was solicited. The service string matches exactly
the one requested. The returned port is the accessible port that the given service
can be accessed on, or `0` if the service is not accessible externally.
