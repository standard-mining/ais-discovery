/*
 * error.rs
 *
 * ais-discovery - Implementation of the AIS Discovery Protocol
 * Copyright (C) 2018 Standard Mining
 *
 * ais-discovery is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * All rights reserved.
 */

use bincode;
use std::error::Error as StdError;
use std::{io, fmt::{self, Write}};

#[must_use]
#[derive(Debug)]
pub enum Error {
    StaticMsg(&'static str),
    Msg(String),
    Io(io::Error),
    Bincode(bincode::Error),
}

impl StdError for Error {
    fn description(&self) -> &str {
        use self::Error::*;

        match *self {
            StaticMsg(s) => s,
            Msg(ref s) => s,
            Io(ref e) => e.description(),
            Bincode(ref e) => e.description(),
        }
    }

    fn cause(&self) -> Option<&StdError> {
        use self::Error::*;

        match *self {
            StaticMsg(_) | Msg(_) => None,
            Io(ref e) => Some(e),
            Bincode(ref e) => Some(e),
        }
    }
}

impl Into<String> for Error {
    fn into(self) -> String {
        if let Error::Msg(string) = self {
            string
        } else {
            let mut string = String::new();
            write!(&mut string, "{}", &self).unwrap();
            string
        }
    }
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", StdError::description(self))?;

        if let Some(cause) = StdError::cause(self) {
            write!(f, ": {}", cause)?;
        }

        Ok(())
    }
}

// Auto-conversion
impl From<String> for Error {
    fn from(error: String) -> Self {
        Error::Msg(error)
    }
}

impl From<io::Error> for Error {
    fn from(error: io::Error) -> Self {
        Error::Io(error)
    }
}

impl From<bincode::Error> for Error {
    fn from(error: bincode::Error) -> Self {
        use self::bincode::ErrorKind::*;

        match *error {
            Io(e) => return Error::Io(e),
            Custom(s) => return Error::Msg(s),
            _ => (),
        }

        Error::Bincode(error)
    }
}
